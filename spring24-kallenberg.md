# Reading Group

Lecture Notes Markov Decision Processes - version 2022

<https://www.researchgate.net/profile/Lodewijk-Kallenberg/publication/359893791_Lecture_Notes_Markov_Decision_Processes_-_version_2022/links/62553afbb0cee02d6965cab0/Lecture-Notes-Markov-Decision-Processes-version-2022.pdf>


Schedule
## Spring 2024

**When**: Thursdays 1-2pm

**Where**: Kinsgsbury N233

| Date   | Topic           | Presenter |
|--------|-----------------|-----------|
| Jan 25 | 1.1,1.2         | Xihong    |
| Feb 1  | 1.3,1.4         | Gersi     |
| Feb 8  | 2.1-2.2         | Maeve     |
| Feb 15 | 2.3-2.4         | Keith     |
| Feb 22 | 2.6             | Ahmed     |
| Feb 29 | 2.5             | Marek     |
| Mar 7  | 3.1             | Monkie    |
| Mar 14 | *No meeting*    |           |
| break  | *No meeting*    |           |
| Mar 28 |  3.2-3.4        | Xihong    |
| Apr 4  | *No meeting*    |           |
| Apr 11 |  3.5-3.7        | Gersi     |
| Apr 18 |  3.8-3.10       | Monkie    |
| Apr 25 |  4.1-4.2        | Marek     |


