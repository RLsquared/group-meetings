# Group Meetings

If you would like to get notifications about the group meetings and other matters, please email [Marek.Petrik@unh.edu](mailto:Marek.Petrik@unh.edu) to be added to the mailing list.

## Spring 2025

**When**: Thursday 11:15-12:30pm
**Where**: Kingsbury N233

**Zoom**: <https://unh.zoom.us/my/marekpetrik>

| Date   | Topic                                                                                                              | Presenter |
|--------|--------------------------------------------------------------------------------------------------------------------|-----------|
| Jan 30 | Ski injuries and career planning                                                                                   |           |
| Feb 06 | [Mitigating the Curse of Horizon](https://rlj.cs.umass.edu/2024/papers/RLJ_RLC_2024_80.pdf)                        | Gersi     |
| Feb 13 | Snow day                                                                                                           |           |
| Feb 20 | ERM Total Reward Criterion                                                                                         | Xihong    |
| Feb 27 | Optimized certainty equivalent                                                                                     | Emily     |
| Mar 06 | [Finite horizon risk sensitive MDP and linear programming](https://ieeexplore.ieee.org/abstract/document/7403457/) | Keith     |
| Mar 13 | Defense practice talk                                                                                              | Monkie    |
| Mar 20 | *Spring break*                                                                                                     |           |
| Mar 27 |                                                                                                                    | Maeve     |
| Apr 03 |                                                                                                                    | Ahmed     |
| Apr 10 |                                                                                                                    | Marek     |
| Apr 17 |                                                                                                                    | Palash    |
| Apr 24 |                                                                                                                    |           |
| May 01 |                                                                                                                    |           |

## Fall 2024

**When**: Thursday 2pm
**Where**: Kingsbury N233

**Zoom**: <https://unh.zoom.us/my/marekpetrik>

| Date   | Topic                                                                                                                      | Presenter       |
|--------|----------------------------------------------------------------------------------------------------------------------------|-----------------|
| Aug 29 | [Risk-Averse Model Uncertainty for Distributionally Robust Safe Reinforcement Learning](https://arxiv.org/abs/2301.12593)  | Palash          |
| Sep 05 | [Efficient Bregman Projections onto the Permutahedron and Related Polytopes](https://proceedings.mlr.press/v51/lim16.html) | Palash          |
| Sep 12 | Depth exam practice talk                                                                                                   | Gersi           |
| Sep 19 | [A New PI for Zero-Sum Markov Games](https://arxiv.org/abs/2303.09716v4)                                                   | Keith           |
| Sep 26 | Optimal transport and Boltzmann machines                                                                                   | Emily           |
| Oct 03 | [Quantile Markov Decision Processes](https://arxiv.org/pdf/1711.05788.pdf)                                                 | Ahmed           |
| Oct 10 | Practice proposal talk                                                                                                     | Xihong          |
| Oct 17 | Proposal practice talk                                                                                                     | Monkie          |
| Oct 24 | Soccer action aggregations                                                                                                 | Charles         |
| Oct 31 | No meeting                                                                                                                 | Maeve           |
| Nov 07 | Another proposal practice                                                                                                  | Monkie          |
| Nov 14 | [Robust Average Reward MDPs](https://www.jair.org/index.php/jair/article/view/15451)                                       | Maeve           |
| Nov 21 | [Constrained Risk-Averse Markov Decision Processes ](https://ojs.aaai.org/index.php/AAAI/article/view/17393)               | Palash          |
| Nov 28 | *Thanksgiving*                                                                                                             |                 |
| Dec 05 | A new approach to off-policy evaluation                                                                                    | Shreyas (UMass) |
	

Papers to discuss in the future

- Xia, L., Zhang, L. and Glynn, P.W. (2023) ‘Risk‐sensitive Markov decision processes with long‐run CVaR criterion’, Production and Operations Management, 32(12), pp. 4049–4067.
- 

## Summer 2024

Implementation meetings


## Spring 2024


**When**: Friday 11am
**Where**: Kingsbury N233

| Date   | Topic                                                                                                                        | Presenter |
|--------|------------------------------------------------------------------------------------------------------------------------------|-----------|
| Feb 2  | [Distributional model equivalence for risk-sensitive reinforcement learning](https://arxiv.org/abs/2307.01708)               | Monkie    |
| Feb 9  | [An analysis of transient Markov decision processes](https://www.jstor.org/stable/27595759)                                  | Xihong    |
| Feb 16 | [An analysis of transient Markov decision processes](https://www.jstor.org/stable/27595759)                                  | Xihong    |
| Feb 23 | [Entropy based risk measures](https://www-sciencedirect-com.unh.idm.oclc.org/science/article/pii/S0377221719300189)          | Marek     |
| Mar 1  | [Entropy based risk measures](https://www-sciencedirect-com.unh.idm.oclc.org/science/article/pii/S0377221719300189)          | Marek     |
| Mar 8  | [Entropy based risk measures](https://www-sciencedirect-com.unh.idm.oclc.org/science/article/pii/S0377221719300189)          | Marek     |
| Mar 15 | Robust imitation learning                                                                                                    | Gersi     |
| Mar 22 | Spring break                                                                                                                 |           |
| Mar 29 | Reinforcement learning for solar generation                                                                                  | Roozbeh   |
| Apr 4  | [Projections onto capped simplex](https://proceedings.neurips.cc/paper/2021/file/52aaa62e71f829d41d74892a18a11d59-Paper.pdf) | Gersi     |
|        |                                                                                                                              |           |


## Fall 2023

**When**: Thursdays 11am - 12pm

**Where**: Kinsgsbury N242

| Date   | Topic                                                                                                               | Presenter |
|--------|---------------------------------------------------------------------------------------------------------------------|-----------|
| Sep 7  | Organizational meeting                                                                                              | Marek     |
| Sep 14 | [Risk-averse RL](https://www.cs.unh.edu/~mpetrik/pub/tutorials/risk2/dlrl2023.pdf)                                  | Marek     |
| Sep 21 | [Inverse RL](https://proceedings.neurips.cc/paper_files/paper/2016/file/cc7e2b878868cbae992d1fb743995d8f-Paper.pdf) | Gersi     |
| Sep 28 | [Distributional RL](https://dl.acm.org/doi/pdf/10.5555/3305381.3305428)                                             | Monkie    |
| Oct 05 | [Risk-averse SSP](https://arxiv.org/pdf/2103.14727.pdf)                                                             | Xihong    |
| Oct 12 | None                                                                                                                |           |
| Oct 19 | Convex RMDPs (practice talk)                                                                                        | Marek     |
| Oct 26 | Risk-averse DP (practice talk)                                                                                      | Monkie    |
| Nov 02 | *No meeting*                                                                                                        |           |
| Nov 09 | Risk management in credit rating and machine learning                                                               | Ahmed     |
| Nov 16 | Hamiltonian methods                                                                                                 | Keith     |
| Nov 23 | *Thanksgiving, no meeting*                                                                                          |           |
| Nov 30 |  Stochastic shortest path                                                                                           | Saba      |
| Dec 07 |  [Risk-sensitive Optimal Control for Markov Decision Processes with Monotone Cost](https://pubsonline.informs.org/doi/pdf/10.1287/moor.27.1.192.334)                                                                                                                        | Xihong    |
| Dec 14 | *No meeting*, Neurips                                                                                               |           |

## Fall 2022

**When**: Thursdays 1pm - 2pm

**Where**: Zoom: https://unh.zoom.us/j/5833938643 

| Date   | Topic                                                                      | Presenter |
|--------|----------------------------------------------------------------------------|-----------|
| Sep 15 | Organizational meeting                                                     | Marek     |
| Sep 22 | Solving Multi-model MDPs                                                   | Xihong    |
| Sep 29 | Markov Decision Processes with Average-Value-at-Risk criteria              | Monkie    |
| Oct 06 | Game-theoretic approach to apprenticeship learning                         | Gersi     |
| Oct 13 | On the linear convergence of policy gradient methods for finite MDPs       | Keith     |
| Oct 20 | Risk-Averse Decision Making Under Uncertainty                              | Saba      |
| Oct 27 | A Game-Theoretic Perspective on Risk-Sensitive Reinforcement Learning      | Harrison  |
| Nov 03 | *No meeting*                                                               | Marek     |
| Nov 10 | [Quantile Markov Decision Processes](https://arxiv.org/pdf/1711.05788.pdf) | Monkie    |
|        |                                                                            |           |

## Summer 2022

**When**: Thursday 12:00pm - 1:00pm

**Where**: Kingsbury N233

Reading convex optimization by Boyd. 

## Spring 2022

**When**: Thursday 10:00am - 11:00am

**Where**: Kingsbury N233

| Date   | Topic                                                    | Presenter                |
|--------|----------------------------------------------------------|--------------------------|
| Feb 10 | General discussion                                       | General meeting          |
| Feb 17 | Practice talk                                            | Monkie        |
| Feb 24 | [On the convergence of policy iteration](https://proceedings.mlr.press/v130/bhandari21a.html)  | Harrison                 |
| Mar 03 | Poisoning policy evaluation                              | Elita                    |
| Mar 10 | RASR                                                     | Monkie                   |
| Mar 17 | Spring break                                             |                          |
| Mar 24 | [Variational Bayesian RL](https://proceedings.neurips.cc/paper/2021/hash/edb446b67d69adbfe9a21068982000c2-Abstract.html)    | Bahram                   |
| Mar 31 | [Entropic MDPs](https://arxiv.org/abs/1705.07798)                                                                           | Marek                    |
| Apr 07 | [Near Optimal Bayesian Ambiguity](https://pubsonline.informs.org/doi/10.1287/mnsc.2018.3140)                                | Marek and Elita          |
| Apr 14 |                                                          | Yash Chandak             |
| Apr 21 |                                                          | Bahram                   |
| Apr 28 |                                                          | Saba                     |
| May 05 |                                                          | Xihong                   |


## Fall 2021

**When**: Wednesday 2:00 - 3:00

**Where**: Kingsbury N233, and [Zoom](https://unh.zoom.us/j/94231222801)


| Date   | Topic                                                                                                              | Presenter   
| ------ | ------------------------------------------------------------------------------------------------------------------ | -------------
|Sep 15  | Proposal practice talk                                                                                             | Bahram       
|Sep 22  | [SQIL](https://arxiv.org/pdf/1905.11108.pdf)                                                                       | Soheil         
|Sep 29  | [Stochastic programs with entropic risk](http://www.optimization-online.org/DB_FILE/2020/08/7984.pdf)              | Monkie        
|Oct 06  | [Generalized IRL for linear MDPs](http://ecmlpkdd2017.ijs.si/papers/paperID171.pdf)                               | Saba           
|Oct 13  | Canceled                                                                                                           |           
|Oct 20  | [Linearly solvable MDPs](https://proceedings.neurips.cc/paper/2006/file/d806ca13ca3449af72a1ea5aedbed26a-Paper.pdf)| Marek
|Oct 27  | [Optimal Off-Policy Evaluation from Multiple Logging Policies](https://arxiv.org/abs/2010.11002)                   | Elita          
|Nov 03  | [Gravitational Pull of Softmax](https://proceedings.neurips.cc/paper/2020/file/f1cf2a082126bf02de0b307778ce73a7-Paper.pdf)   | Gerry    
|Nov 10  | [Optimality of Policy Gradient](https://arxiv.org/pdf/1908.00261.pdf)                                              | Soheil
|Nov 17  | [Multimodel MDP](http://www.optimization-online.org/DB_FILE/2018/01/6434.pdf)                                      | Xihong         
|Nov 24  |                                                                                                                    | Thanksgiving   
|Dec 01  | [Accelerated VI](http://proceedings.mlr.press/v139/farahmand21a/farahmand21a.pdf)                                  | Harrison          
|Dec 08  |                                                                                                                    | Neurips        


## Spring 2021

**When**: Thursday 9:30 - 10:30

**Where**: Zoom

| Date | Topic                                                                      | Presenter|
| ---- | -------------------------------------------------------------------------- | -------- |
| 1/28 | [Algorithmic imitation learning](https://doi.org/10.1561/2300000053)       | Soheil   |
| 2/04 | Open problem: Convex formulation RMDP                                      | Marek    |
| 2/11 | [BROIL](https://arxiv.org/abs/2007.12315)                                  | Gerry    |
| 2/18 | [Policy poisoning in batch RL](https://arxiv.org/abs/1910.05821)           | Elita    |
| 2/25 | [Distributionally Robust MDPs](https://www.jstor.org/stable/23256624)      | Monkie   |
| 3/04 | [Offline RL](https://arxiv.org/abs/2005.01643)                             | Bahram   |
| 3/11 | [Epistemic Risk-Sensitive RL](https://arxiv.org/pdf/1906.06273.pdf)        | Reazul   |
| 3/18 | Break                                                                      |          |
| 3/25 | Break                                                                      |          |
| 4/01 | [Reward machines](http://proceedings.mlr.press/v80/icarte18a/icarte18a.pdf)| Colin    |
| 4/08 | Break                                                                      |          |
| 4/15 | [Resource-constrained DRL](https://ojs.aaai.org/index.php/ICAPS/article/view/3528/3396)  | Elita    |
| 4/22 | [Statistical limits of offline RL](https://arxiv.org/abs/2010.11895)       | Saba     |
| 4/29 | Break                                                                      |          |
| 5/06 | [IRL via Classificiation](http://www.lifl.fr/~pietquin/pdf/NIPS_2012_EKBPMGOP.pdf)   | Gerry    |

Other papers:

 - [Reinforcement Learning with Convex Constraints](https://papers.nips.cc/paper/2019/file/873be0705c80679f2c71fbf4d872df59-Paper.pdf)

### Progress update meetings: 

**When**: Tuesday 9:30 - 11:30

1. Short update: Anyone present: 5 minutes
2. Long update: up to 20 minutes including discussion: 

Long updates: even weeks: Elita, Monkie, Reazul, odd weeks: Bahram, Soheil, Devin



## Fall 2020

**When**: Tuesday 12pm

**Where**: Zoom

| Date    | Topic                                    | Presenter        |
| ------- | ---------------------------------------- | ---------------- |
| 09/22   | [Accelerated Value Iteration](https://arxiv.org/abs/1905.09963)  | Marek
| 09/29   | [Bias and Variance Approximation in Value Function Estimates](https://pubsonline.informs.org/doi/abs/10.1287/mnsc.1060.0614)     | Monkie   
| 10/06   |                                          | Soheil
| 10/13   |                                          | Colin   
| 10/20   | [State aggregation learning](http://arxiv.org/abs/1811.02619)     | Sepideh
| 10/27   | Proposal?                                | Devin
| 11/03   | Proposal practice                        | Reazul
| 11/10   | Application!                             | Marek
| 11/17   | Proposal practice                        | Chris
| 11/24   | Proposal practice                        | Bahram

## Summer 2020

**When**: Wednesday 9am - 10am

**Where**: Zoom (mailing list for link)

| Date | Topic                                                                                                                                    | Presenter |
|------|------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| 5/27 | Inverse RL and Robustness                                                                                                                | Soheil    |
| 6/03 | Optimizer's Curse                                                                                                                        | Lynette   |
| 6/10 | [Risk and Robustness CVaR](https://papers.nips.cc/paper/6014-risk-sensitive-and-robust-decision-making-a-cvar-optimization-approach.pdf) | Monkie    |
| 6/17 | [Deep RL and Energy](https://arxiv.org/abs/1702.08165)                                                                                   | Saba      |
| 6/24 | Polyanimals project                                                                                                                      | Chris     |
| 7/01 | [Interpretable off-policy eval](https://arxiv.org/pdf/2002.03478.pdf)                                                                    | Elita     |
| 7/08 | Lasso                                                                                                                                    | Devin     |
| 7/15 | [Safe Policy Improvement](https://arxiv.org/abs/1712.06924)                                                                              | Bahram    |
| 7/22 | Inverse RL                                                                                                                               | Mostafa   |
| 7/29 | Review                                                                                                                                   | Marek     |


## Spring 2020

**When**: Wednesday 3:30pm - 4:30pm

**Where**: Kingsbury N242

Ski-trip: Sat Feb 15th, 2020


| Date   | Topic                                                                                                                                                                                  | Presenter |
|--------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| Jan 29 | [Intrinsically Efficient ... Off-Policy Evaluation](https://papers.nips.cc/paper/8594-intrinsically-efficient-stable-and-bounded-off-policy-evaluation-for-reinforcement-learning.pdf) | Bahram    |
| Feb 05 | [Percentile Optimization for MDPs](https://pubsonline.informs.org/doi/abs/10.1287/opre.1080.0685)                                                                                      | Monkie    |
| Feb 12 | Proposal: Robust POMDPs                                                                                                                                                                | Devin     |
| Feb 19 | Robust IRL                                                                                                                                                                             | Soheil    |
| Feb 26 | Proposal: Spruce Budworm                                                                                                                                                               | Jason     |
| Mar 04 | [Policy Gradient for Coherent Risk Measures](https://arxiv.org/pdf/1502.03919.pdf)                                                                                                     | Talha     |
| Mar 11 | Poly-animals                                                                                                                                                                           | Chris     |
| Mar 18 | *Spring break*                                                                                                                                                                         |           |

Coronavirus .....

## Fall 2019

**When**: Thursday 11:00am - 12:30pm, some days 10:30 - 12:00

**Where**: Kingsbury N233


| Date   | Topic                             | Presenter 
| ------ | --------------------------------- | --------- 
| Sep 19 | [Fingerprint policy optimization](https://arxiv.org/abs/1805.10662)                                                     | Bahram 
| Sep 26 | [Action Robust Reinforcement Learning](https://arxiv.org/pdf/1901.09184.pdf)                                            | Shayan  
| Oct 03 | Robust Risk-Averse Sequential Decision Making                                                                           | Jason 
| Oct 10 | Optimizing Norm-bounded Weighted Ambiguity Sets for Robust MDPs                                                         | Reazul 
| Oct 17 | [POMDPs easy to approximate](https://papers.nips.cc/paper/3291-what-makes-some-pomdp-problems-easy-to-approximate.pdf)  | Monkie 
| Oct 24 | [MaxEnt IRL](https://www.aaai.org/Papers/AAAI/2008/AAAI08-227.pdf)                                                      | Soheil
| Oct 31 | [Point-based value iteration: An anytime algorithm for POMDPs](https://www.ijcai.org/Proceedings/03/Papers/147.pdf)     | Xihong 
| Nov 07 | [Likelihood analysis of species occurrence probability from presence-only data for modelling species distributions](https://besjournals.onlinelibrary.wiley.com/doi/full/10.1111/j.2041-210X.2011.00182.x)                                  | Chris 
| Nov 14 | [Concentration Inequalities for CVaR](https://people.cs.umass.edu/~pthomas/papers/Thomas2019.pdf)                       | Talha 
| Nov 21 |                                   | Rasel  
| Nov 28 | *Thanksgiving*                    | 
| Dec 05 |                                   | Marek 


## Summer 2019

**When**: Wednesday 12:30pm - 1:30pm

**Where**: Kingsbury N233

**Also**: Pizza

| Date  | Topic                                                                                                                           | Presenter|
| ----- | --------------------------------------------------------------------------------------------------------------------------------| -------- |
| May 29| [Near-OptimalBayesianAmbiguity Sets](http://www-bcf.usc.edu/~guptavis/Papers/Bayes_DRO_v5_final.pdf)                            | Marek
| Jun 05| [Uncertainty Analysis of Species Distribution Models](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0214190)| Saba
| Jun 12| Robust and risk averse MDPs                                                                                                     | Jason
| Jun 19| [Deep Reinforcement Learning ...](http://proceedings.mlr.press/v80/lee18b/lee18b.pdf)                                           | Xihong
| Jun 26|                                                                                                                                 | Bahram
| Jul 03|                                                                                                                                 | Monkey & Talha
| Jul 17|                                                                                                                                 | Reazul
| Jul 24|                                                                                                                                 | Chris or Soheil
| Jul 31|                                                                                                                                 | Jordan

## Spring 2019

**When**: Monday 3:30pm - 4:30pm

**Where**: Kingsbury N242

| Date   | Topic                                                                                               | Presenter  |
| ------ | ----------------------------------------------------------------------------------------------------| ---------  |
| Jan 28 | Introduction                                                                                        | Marek
| Feb 04 | Optimism in the Face of Value Functions                                                             | Reazul and Tianyi
| Feb 11 | [Adversarial Attacks on Neural Policies](https://arxiv.org/abs/1702.02284)                          | Shayan
| Feb 18 | [Performance Loss Bounds for Approximate Value Iteration with State Aggregation](https://dl.acm.org/citation.cfm?id=1246333)                                                      | Soheil
| Feb 25 | [Near-OptimalBayesianAmbiguity Sets](http://www-bcf.usc.edu/~guptavis/Papers/Bayes_DRO_v5_final.pdf)| Bahram
| Mar 04 | R-rectangular                                                                                       | Saba
| Mar 11 | **Spring Break**                                                                                    |
| Mar 18 | Greenhouse and other stuff                                                                          | Sangeeta or Chris
| Mar 25 | Robustness to confounding factors                                                                   | Monkie
| Apr 01 | Bear world                                                                                          | Chris
| Apr 08 | MMS                                                                                                 | Samantha
| Apr 15 | Canceled                                                                                            | 
| Apr 22 | Original results                                                                                    | Reazul
| Apr 29 | Distributional heuristic functions                                                                  | Tianyi
| May 06 | SBEED                                                                                               | Marek



## Fall 2018

**When**: Monday 2:00pm - 3:00pm

**Where**: Kingsbury N233


| Date  | Day |              Topic              | Presenter |
| ----- | --- | :-----------------------------: |  :------: |
|Sep 10 | Mon | Summer Review                   |           |
|Sep 17 | Mon | [Planning in the Presence of Cost Functions Controlled by an Adversary](http://www.cs.cmu.edu/~ggordon/mcmahan-ggordon-blum.icml2003.pdf)     |  Jason    |
|Sep 24 | Mon | Bayesian Distributional Robustness                               |  Marek    |
|Oct 01 | Mon | [A data-driven, machine learning framework for optimal pest management in cotton](https://esajournals.onlinelibrary.wiley.com/doi/full/10.1002/ecs2.1263)  |  Chris    |
|Oct 09 | Tue | Policy Conditioned Uncertainty Sets for Robust MDPs                 |  Bahram   |
|Oct 15 | Mon | *Canceled*                               |           |   
|Oct 22 | Mon |  [(More) Efficient Reinforcement Learning via Posterior Sampling](https://arxiv.org/pdf/1306.0940.pdf)                                |  Tianyi   |
|Oct 29 | Mon | [Reinforcement learning with basis construction ...](https://www.sciencedirect.com/science/article/pii/S0020025514006987)               |  Soheil   |
|Nov 05 | Mon | Finding Magnetopause            |  Samantha |
|Nov 12 | Mon | Veteran's day                   |  
|Nov 19 | Mon | Bayesian robust RL              |  Reazul   |
|Nov 26 | Mon | Optimization for Greenhouses    |  Chris   |
|Dec 03 | Mon | Canceled                        |   |
|Dec 10 | Mon | NIPS report                     | Marek and Reazul |



## Summer 2018

**When**: Wednesday 12:00pm - 1:00pm

**Where**: Kingsbury N233

**Also**: Pizza


Date    |     Speaker       |     Topic         |
--------|-------------------|-------------------|
May 16  | Marek (Andreas)   | Computing Robust Strategies for Managing Invasive Plants |
May 23  | Marek             | Fast Robust Bellman Updates |
May 30  | Coral (Rachel)    | [Autonomous Vehicle Navigation in Rural Environments without Detailed Prior Maps](https://toyota.csail.mit.edu/sites/default/files/documents/papers/ICRA2018_AutonomousVehicleNavigationRuralEnvironment.pdf) |
June 6  | Bahram (Jordan)   | [Low-Rank Value Function Approximation for Co-optimization of Battery Storage](https://ieeexplore.ieee.org/abstract/document/7950964/)                |
June 13 | Jason (Shayan)    | [Minimax Regret Bounds for Reinforcement Learning](https://arxiv.org/pdf/1703.05449) |
June 20 | Monkie            | MDPs for Playing Blackjack |
June 27 | Shayan            | MaxLike for Species Distribution Models |
July 4  |                   | *No meeting*                          |



## Spring 2018


**When**: Wednesday 4:00pm - 5:00pm

**Where**: Kingsbury N233

**Also**: Refreshments


Date    |     Speaker       |     Topic         |
--------|-------------------|-------------------|
Feb 21  | Bahram            | Sparsity and LASSO
Feb 28  | Reazul            | [Robust Adversarial Reinforcement Learning](http://proceedings.mlr.press/v70/pinto17a/pinto17a.pdf)
Mar  21 | Jordan            | [Robust and Efficient Transfer Learning with Hidden Parameter Markov Decision Processes](https://arxiv.org/abs/1706.06544)
Mar 28  | Coral             | [Adaptive Neural Network Control of AUVs With Control Input Nonlinearities Using Reinforcement Learning](http://ieeexplore.ieee.org/abstract/document/7812772/)
Apr 4   | Shayan            | [Safe Policy Improvement by Minimizing Robust Baseline Regret](http://rmdp.xyz/publications/pub/Petrik2016b.pdf)
Apr 11  | Marek             | Risk-aversion tutorial
Apr 18  | Rachel            | [PAC Optimal MDP Planning with Application to Invasive Species Management](http://www.jmlr.org/papers/volume16/taleghan15a/taleghan15a.pdf)
Apr 25  | Jason             | [Safe Policy Improvement with Baseline Bootstrapping](https://arxiv.org/abs/1712.06924)
May 2   | Bahram            | Markov decision processes
May 9   | Marek             | How to be a graduate student


## Winter 2018

Joint meetings with Wheeler's group.

**When**: Wednesday 12:00pm - 1:00pm

**Where**: Kingsbury N233

**Also**: Pizza

Date    |    Paper          |
--------|-------------------|
Jan 3   |  [DESPOT](http://jair.org/papers/paper5328.html)                                  |
Jan 10  |  [Robust MDPs](https://pubsonline.informs.org/doi/abs/10.1287/opre.1050.0216)     |
Jan 17  |  [TDTS](http://www.jair.org/papers/paper5507.html)                 |

## Fall 2017

**When**: Friday 10:00am - 11:00am

**Where**: Kingsbury N233


Date    |     Speaker       |     Topic         |
--------|-------------------|-------------------|
|Sep 01 | Marek             | Fast s-rectangular robustness
|Sep 08 | Bahram            | [Linear feature encoding](https://users.cs.duke.edu/~parr/NIPS2016_Encoder.pdf)| 
|Sep 15 | Bence             | Meeting canceled, Bence at robotics seminar: 12:10-1pm in Kingsbury N101  | 
|Sep 22 | Jordan            | Meta-modeling and spatial analysis | 
|Sep 29 | (Hala)            | | 
|Oct 06 | Reazul            | Bayesian robust ambiguity sets     | 
|Oct 13 | Andreas           | [Maximum entropy for species distributions](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.187.89&rep=rep1&type=pdf) | 
|Oct 20 | Marek             | [Minimizing Regret in MDPs](https://www.jair.org/media/5242/live-5242-10005-jair.pdf) | 
|Oct 27 | Bence + Jordan    | Active learning and MaxEnt | 
|Nov 03 | Talha             | [Practical Precautionary Resource Management Using Robust Optimization](https://link.springer.com/article/10.1007/s00267-014-0348-1) | 
|Nov 10 |                   | **Veteran's day** | 
|Nov 17 | Nobody            | | 
|Nov 24 |                   | **Thanksgiving** | 
|Dec 01 | Jordan            | | 
|Dec 08 | Bence             | [LinUCB](https://arxiv.org/pdf/1003.0146.pdf) | 
